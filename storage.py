def create_bucket():  
    ''' Create Bucket with unique identifier

        return bucket_name
     '''
    # Create the service client.
    from googleapiclient.discovery import build
    gcs_service = build('storage', 'v1')

    # Generate a random bucket name to which we'll upload the file.
    import uuid
    bucket_name = 'colab-bucket' + str(uuid.uuid1())

    body = {
        'name': bucket_name,
        # For a full list of locations, see:
        # https://cloud.google.com/storage/docs/bucket-locations
        'location': 'us',
    }
    gcs_service.buckets().insert(project=project_id, body=body).execute()
    print('Done')
    return bucket_name


def cp_to_bucket(bucket_name, file_path, upload_file_name):
    '''Copy file from local drive to storage bucket

    # Arguments
        bucket_name: Name of the storage bucket
        file_path: local path
        upload_file_name: Name of file to upload

    '''

    from googleapiclient.http import MediaFileUpload

    media = MediaFileUpload(file_path, 
                            mimetype='text/plain',
                            resumable=True)

    # Create the service client.
    from googleapiclient.discovery import build
    gcs_service = build('storage', 'v1')
    request = gcs_service.objects().insert(bucket=bucket_name, 
                                            name=upload_file_name,
                                            media_body=media)

    response = None
    while response is None:
        # _ is a placeholder for a progress object that we ignore.
        # (Our file is small, so we skip reporting progress.)
        _, response = request.next_chunk()

    print('Upload complete')
  
  
  
def cp_from_bucket(bucket_name, file_name, download_file_path):
  '''Copy file from storage bucket to local folder

    # Arguments
        bucket_name: Name of the bucket where file is saved
        file_name: Name of the file to copy
        download_file_path: Where to save file locally
  '''

  # Create the service client.
  from googleapiclient.discovery import build
  gcs_service = build('storage', 'v1')
  from apiclient.http import MediaIoBaseDownload

  with open(download_file_path, 'wb') as f:
    request = gcs_service.objects().get_media(bucket=bucket_name,
                                              object=file_name)
    media = MediaIoBaseDownload(f, request)

    done = False
    while not done:
      # _ is a placeholder for a progress object that we ignore.
      # (Our file is small, so we skip reporting progress.)
      _, done = media.next_chunk()

  print('Download complete')